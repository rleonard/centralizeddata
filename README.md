CentralizedData
===============

A project to allow flexible views of staff and student posts, such as website entries or standard blog posts, across platforms and individual websites.

Downloading
-----------

Download the repo though git (`git pull https://rleonard@bitbucket.org/rleonard/centralizeddata.git CentralizedData`) or download a ZIP of the [latest code](https://bitbucket.org/rleonard/centralizeddata/downloads), or download a specific release version by clicking on "Tags".

Installing
----------

Once the source files have been downloaded, Node.js modules need to be added though the `npm` package manger.

	npm install

[MongoDB](http://www.mongodb.org/) also needs to be downloaded and installed.  Packages are avalible for most Linux systems.  On Ubuntu, use `[sudo] apt-get install mongodb`.  MongoDB should be running before the application is started, although most packages will be set to automatically start, so no additional actons are required.

After the packages have been downloaded, the source can be built.  To build without running, use

	make app.js

Or just run the files, and any compilation needed will be executed:

	make run

Running
-------

CentralizedData can be run though make:

	make run

which will compile any needed files and run the server.

Alternatively, after CentralizedData has been compiled once,

	node app.js

will run the server, but any missing files that aren't compiled won't be built.

Configuring
-----------

Most of the configuration options are set by default in `/src/config.coffee`.  However, a few configuration variables are required for the code to work, and can't be provided by default.

Before using the website, please create the file `config.json`, and add the details for Google's OAuth.  A sample is below.

	{
		"googleOAuth": {
			"id": "...",
			"secret": "...",
			"anonFetchRedirectURL": "http://localhost:8080/oauth2callback",
			"userLoginRedirectURL": "http://localhost:8080/login/google/auth/",
			"anonFetchRedirectPath": "/oauth2callback",
			"userLoginRedirectPath": "/login/google/auth/"
		}
	}

We also recommend changing `web.port` to 80 if you are running CentralizedData as a primary web server (set to 8080 by default), and changing the mongo database location.

		"web": {
			"port": 80
		}

All other configuration options can be found in `/src/config.coffee` starting on the third line.

Forking
-------

If you fork the code, please make sure you change the email address in the default configuration file (`/src/config.coffee`) to your own.  Feel free to pass along relevant issues, but please make sure that the issues aren't with any changes in the fork.