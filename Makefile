run: app.js
	node app.js

supporting_files.js: src/*.coffee
	coffee -b -j supporting_files.js -c src/*.coffee

main.js: app.coffee
	coffee -b -j main.js -c app.coffee

app.js: main.js supporting_files.js launcher.js
	cat main.js supporting_files.js launcher.js > app.js