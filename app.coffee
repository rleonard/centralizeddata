us = {ryanleonard: {}}
class us.ryanleonard.CentralizedData
	constructor: ->
		@log = new us.ryanleonard.CentralizedData.Logger @
		@config = new us.ryanleonard.CentralizedData.Config @
		@database = new us.ryanleonard.CentralizedData.Mongoose @
		@google = new us.ryanleonard.CentralizedData.GoogleAPI @
		@parser = new us.ryanleonard.CentralizedData.Blade @
		@router = new us.ryanleonard.CentralizedData.Express @
		@fetcher = new us.ryanleonard.CentralizedData.BlogFetcher @
		@login = new us.ryanleonard.CentralizedData.Login @
		@homepage = new us.ryanleonard.CentralizedData.Overview @
		@subscriptions = new us.ryanleonard.CentralizedData.Subscriptions @
		@publish = new us.ryanleonard.CentralizedData.Publish @
		@router.listen()

#main = new us.ryanleonard.CentralizedData()
