class us.ryanleonard.CentralizedData.Subscriptions
	constructor: (@main) ->
		@main.router.get "/sub/", @checkUserAuth, @showSubs
		@main.router.get "/sub/:id/delete/", @checkUserAuth, @deleteSub
		@main.router.get "/sub/:id/fetch/", @checkUserAuth, @fetchSub
		@main.router.post "/sub/:id/update/", @checkUserAuth, @updateSub
		@main.router.post "/sub/create/", @checkUserAuth, @createSub
	checkUserAuth: (req, res, next) =>
		if req.session.googleId and req.session.displayName
			next()
		else
			res.redirect "/login/"
	showSubs: (req, res, next) =>
		@main.database.BloggerSubscription
			.find({localUser: req.session.localUserId})
			.exec (err, subs) =>
				res.render "subList",
					siteTemplate: @main.config.get "template"
					googleId: req.session.googleId
					displayName: req.session.displayName
					subs: subs
	deleteSub: (req, res, next) =>
		sub = req.params.id
		@main.database.BloggerSubscription
			.find({_id: sub, localUser: req.session.localUserId})
			.remove (err) ->
				res.send 200, "Success.  (Error: #{err})"
	fetchSub: (req, res, next) =>
		sub = req.params.id
		@fetchSubById req, res, sub
	fetchSubById: (req, res, sub, reply = yes) ->
		@main.database.BloggerSubscription
			.findOne({_id: sub, localUser: req.session.localUserId})
			.exec (err, sub) =>
				if err
					res.send 500
				else
					oauth2Client = @main.google.userClient()
					oauth2Client.setCredentials req.session.oauth
					oauth2Client.refreshAccessToken (err, tokens) =>
						blogger = @main.google.google.blogger
							version: "v3"
							auth: oauth2Client
						@main.fetcher.processBlog res, blogger, {id: sub.url}, (data) =>
							item = data.item
							item.addedBy = req.session.localUserId
							item.bloggerSubscription = sub._id
							@main.database.BloggerSubscription.findOneAndUpdate
									_id: sub
									localUser: req.session.localUserId
								,
									title: data.blogInfo.name
								, (err, bloggerSubscription) =>
									if err
										console.log err
										res.send 500
									else
										console.log JSON.stringify bloggerSubscription
										#bloggerSubscription.items.push item
										#bloggerSubscription.save (err) ->
										#	if reply
										#		res.send 200
	updateSub: (req, res, next) =>
		sub = req.params.id
		title = req.body.title
		url = req.body.url
		@main.database.BloggerSubscription
			.findOneAndUpdate
					_id: sub
					localUser: req.session.localUserId
				,
					title: title
					url: url
				, (err, bloggerSubscription) ->
					res.send 200, "Success.  (Error #{err})"
	createSub: (req, res, next) =>
		subs = req.body.subscriptions.split ","
		for sub in subs
			bloggerSubscription = new @main.database.BloggerSubscription
				localUser: req.session.localUserId
				url: sub
				dateAdded: new Date().getTime()
			bloggerSubscription.save (err) =>
				if !err
					@fetchSubById req, res, bloggerSubscription._id, no