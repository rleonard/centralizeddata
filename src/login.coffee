class us.ryanleonard.CentralizedData.Login
	constructor: (@main) ->
		config = @main.config.get "googleOAuth"
		@main.router.get "/login/", @loginPage
		@main.router.get "/login/google/", @googleLogin
		@main.router.get config.userLoginRedirectPath, @googleResponse
		@main.router.get "/logout/", @logout
	loginPage: (req, res, next) =>
		res.redirect "/login/google/"
	googleLogin: (req, res, next) =>
		url = @main.google.userClient().generateAuthUrl
			access_type: "online"
			scope: [
				"https://www.googleapis.com/auth/userinfo.profile"
				"https://www.googleapis.com/auth/userinfo.email"
				"https://www.googleapis.com/auth/blogger"
				"https://www.googleapis.com/auth/blogger.readonly"
			]
		res.redirect url
	googleResponse: (req, res, next) =>
		code = req.param "code"
		@main.google.userClient().getToken code, (err, tokens) =>
			if err
				@main.log.error @, "Error logging in user", err, res
			else
				req.session.oauthCode = code
				req.session.oauth = tokens
				@getUserInfo tokens,
					(err) =>
							@main.log.error @, "Error getting user info", err, res, {tokens: tokens}
					, (googleUserInfo) =>
						@setupGoogleUser req, res, tokens, googleUserInfo
	setupGoogleUser: (req, res, tokens, googleUserInfo) ->
		req.session.googleId = googleUserInfo.id
		req.session.displayName = googleUserInfo.name
		@createGoogleUser googleUserInfo,
			(err) =>
				@main.log.error @,
					"Error creating Google user in database",
					err, res,
						googleUserInfo: googleUserInfo
			, (googleUser) =>
				@fetchBloggerUser req, res, tokens, googleUserInfo, googleUser
	fetchBloggerUser: (req, res, tokens, googleUserInfo, googleUser) ->
		@getBloggerUserInfo tokens,
			(err) =>
				@main.log.error @,
					"Error getting Blogger user info",
					err, res,
						googleUserInfo: googleUserInfo
			, (bloggerUserInfo) =>
				@setupBloggerUser req, res, googleUserInfo, googleUser, bloggerUserInfo
	setupBloggerUser: (req, res, googleUserInfo, googleUser, bloggerUserInfo) ->
		@createBloggerUser bloggerUserInfo,
			(err) =>
				@main.log.error @,
					"Error create Blogger user in database",
					err, res,
						googleUserInfo: googleUserInfo
						bloggerUserInfo: bloggerUserInfo
			, (bloggerUser) =>
				@setupLocalUser req, res, googleUserInfo, googleUser, bloggerUserInfo, bloggerUser
	setupLocalUser: (req, res, googleUserInfo, googleUser, bloggerUserInfo, bloggerUser) ->
		@createLocalUser googleUserInfo, googleUser,
			bloggerUser,
			(err) =>
				@main.log.error @,
					"Error creating local user in database",
					err, res,
						googleUserInfo: googleUserInfo
						bloggerUserInfo: bloggerUserInfo
			, (localUser) =>
				req.session.localUserId = localUser._id
				res.redirect "/"
	getUserInfo: (tokens, errCB, successCB) ->
		oauth2Client = @main.google.userClient()
		oauth2Client.setCredentials tokens
		oauth = @main.google.google.oauth2
			version: "v2"
			auth: oauth2Client
		oauth.userinfo.get {}, (err, googleUserInfo) ->
			if err
				errCB err
			else
				successCB googleUserInfo
	createGoogleUser: (googleUserInfo, errCB, successCB) ->
		@main.database.GoogleUser.findOneAndUpdate
				googleId: googleUserInfo.id
			, googleUserInfo,
				upsert: true
			, (err, googleUser) ->
				if err
					errCB err
				else
					successCB googleUser
	getBloggerUserInfo: (tokens, errCB, successCB) ->
		oauth2Client = @main.google.userClient()
		oauth2Client.setCredentials tokens
		blogger = @main.google.google.blogger
			version: "v3"
			auth: oauth2Client
		blogger.users.get {userId: "self"}, (err, bloggerUserInfo) ->
			if err
				errCB err
			else
				successCB bloggerUserInfo
	createBloggerUser: (bloggerUserInfo, errCB, successCB) ->
		@main.database.BloggerUser.findOneAndUpdate
				id: bloggerUserInfo.id
			, bloggerUserInfo,
				upsert: true
			, (err, bloggerUser) ->
				if err
					errCB err
				else
					successCB bloggerUser
	createLocalUser: (googleUserInfo, googleUser, bloggerUser, errCB, successCB) ->
		@main.database.LocalLogin.findOneAndUpdate
				googleId: googleUserInfo.id
			,
				googleId: googleUserInfo.id
				googleUser: googleUser._id
				bloggerUser: bloggerUser._id
			,
				upsert: true
			, (err, localUser) ->
				if err
					errCB err
				else
					successCB localUser
	logout: (req, res, next) ->
		req.session.googleId = null
		req.session.displayName = null
		res.redirect "/"
	checkUserAuth: (req, res, next) =>
		if req.session.googleId and req.session.displayName
			next()
		else
			res.redirect "/login/"