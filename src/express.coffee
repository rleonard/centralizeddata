class us.ryanleonard.CentralizedData.Express
	constructor: (@main) ->
		@express = require "express"
		@session = require "express-session"
		@sessionStore = require("mongoose-session")(@main.database.mongoose)
		@app = @express()
		@loadBodyParser()
		@loadSession()
		@loadStatic()
		@main.parser.loadMiddleware(@app) # TODO
	loadBodyParser: ->
		bodyParser = require "body-parser"
		@app.use bodyParser.json()
		@app.use bodyParser.urlencoded() 
	loadSession: ->
		config = @main.config.get "session"
		@app.use @session
			secret: config.secret
			store: @sessionStore
			cookie: config.cookie
	loadStatic: ->
		@app.use @express.static __dirname + "/public"
	get: (path, cb...) ->
		@app.get path, cb...
	post: (path, cb...) ->
		@app.post path, cb...
	listen: ->
		@app.listen @main.config.get("web").port