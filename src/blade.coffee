class us.ryanleonard.CentralizedData.Blade
	constructor: (@main) ->
		@blade = require "blade"
	loadMiddleware: (app) ->
		app.use @blade.middleware __dirname + "/blade"
		app.set "views", __dirname + "/blade"
		app.set "view engine", "blade"