class us.ryanleonard.CentralizedData.Mongoose
	constructor: (@main) ->
		@mongoose = require "mongoose"
		@connect()
		@loadModels()
	connect: ->
		config = @main.config.get "db"
		@mongoose.connect "mongodb://#{config.host}/#{config.table}"
	loadModels: ->
		@Schema = @mongoose.Schema
		@Item = @model "Item", @itemSchema()
		@GoogleUser = @model "GoogleUser", @googleUserSchema()
		@BloggerUser = @model "BloggerUser", @bloggerUserSchema()
		@LocalLogin = @model "LocalLogin", @localLoginSchema()
		@BloggerSubscription = @model "BloggerSubscription", @bloggerSubscriptionSchema()
	model: (name, schema) ->
		if !schema.options
			schema.options = @Schema.options
		@mongoose.model name, schema
	itemSchema: ->
		new @Schema
			source: String
			addedBy: {type: @Schema.Types.ObjectId, ref: "LocalLogin"}
			blogDetails:
				title: String
				url: String
			BloggerSubscription: {type: @Schema.Types.ObjectId, ref: "BloggerSubscription"}
			id: String
			title: String
			author:	{type: @Schema.Types.ObjectId, ref: "BloggerUser"}
			link: String
			content: String
			publishDate: Date
			published: Boolean
			featured: Boolean
	bloggerUserSchema: ->
		new @Schema
			id: String
			kind: String
			created: String
			displayName: String
			url: String
			selfLink: String
			image:
				url: String
			about: String
			items: [{type: @Schema.Types.ObjectId, ref: "Item"}]
	googleUserSchema: ->
		new @Schema
			id: String
			email: String
			verified_email: String
			name: String
			given_name: String
			family_name: String
			link: String
			picture: String
			locale: String
	localLoginSchema: ->
		new @Schema
			googleId: String
			refreshToken: String
			googleUser: {type: @Schema.Types.ObjectId, ref: "GoogleUser"}
			bloggerUser: {type: @Schema.Types.ObjectId, ref: "BloggerUser"}
			subscriptions: {type: @Schema.Types.ObjectId, ref: "BloggerSubscription"}
	bloggerSubscriptionSchema: ->
		new @Schema
			localUser: {type: @Schema.Types.ObjectId, ref: "LocalLogin"}
			title: String
			url: String
			dateAdded: String
			items: {type: @Schema.Types.ObjectId, ref: "Item"}