class us.ryanleonard.CentralizedData.Logger
	constructor: (@main) ->
		@fs = require "fs"
		@load()
	warning: (obj, message, err) ->
		console.log "Warning in #{obj.constructor.name}: #{message}; #{err}"
		@logged.push
			level: "warning"
			source: obj.constructor.name
			userMessage: message
			errorMessage: err
			time: new Date()
		@save()
	error: (obj, message, err, res, data) ->
		timestamp = new Date()
		console.log "#{timestamp}: Error in #{obj.constructor.name}: #{message}; #{err}"
		@logged[@logged.length] =
			level: "error"
			source: obj.constructor.name
			userMessage: message
			errorMessage: err
			time: timestamp
			information: data
		res.render "error",
			siteTemplate: @main.config.get "template"
			googleId: null
			displayName: null
			error: message
			number: @logged.length - 1
			escapedErrorId: encodeURIComponent @logged.length - 1
			escapedMessage: encodeURIComponent message
			escapedTimestamp: encodeURIComponent timestamp
			errorAddress: @main.config.get "supportEmail"
		@save()
	load: ->
		try
			@logged = require "./log.json"
		catch err
			@logged = []
			@warning @, "Log file empty! Well, there's an error.  Not so empty anymore.  Error fetching contents of log.json", err
	save: ->
		@fs.writeFile "log.json", JSON.stringify @logged