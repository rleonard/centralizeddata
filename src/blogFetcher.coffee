class us.ryanleonard.CentralizedData.BlogFetcher
	anonTokens = no
	constructor: (@main) ->
		@fs = require "fs"
		config = @main.config.get "googleOAuth"
		@main.router.get "/fetch/", @checkAnonAuth, @fetch
		@main.router.get config.anonFetchRedirectPath, @anonAuth
	checkAnonAuth: (req, res, next) =>
		if not @anonTokens
			url = @main.google.anonClient().generateAuthUrl
				access_type: "online"
				scope: [
					"https://www.googleapis.com/auth/blogger"
				]
			res.redirect url
		else
			next()
	fetch: (req, res, next) =>
		oauth2Client = @main.google.anonClient()
		oauth2Client.setCredentials @anonTokens
		blogger = @main.google.google.blogger
			version: "v3"
			auth: oauth2Client
		blogs = @anonBlogs()
		for blog, blogIndex in blogs
			blogs[blogIndex] = @processBlog res, blogger, blog
		#@writeAnonBlogs blogs
		res.redirect "/"
	processBlog: (res, blogger, blog, cb=null) ->
		blogger.blogs.getByUrl {url: blog.id}, (err, blogInfo) =>
			if err
				@main.log.error @, "Error fetching blog", err, res
			else
				@listPosts res, blogger, blog, blogInfo, cb
	listPosts: (res, blogger, blog, blogInfo, cb) ->
		opts =
			blogId: blogInfo.id
			fetchBodies: true
		if blog.lastGrab
			opts.startDate = blog.lastGrabRFC3339
		blogger.posts.list opts, (err, response) =>
			if err
				@main.log.error @, "Error fetching posts", err, res
			else
				for post, postNum in response.items
					@createPostAuthor res, blogger, blog, blogInfo, post, cb
	createPostAuthor: (res, blogger, blog, blogInfo, post, cb) ->
		{id, title, author, content, published, url} = post
		publishDate = Date.parse published
		@main.database.BloggerUser.findOneAndUpdate
				id: author.id
			, author,
				upsert: true
			, (err, bloggerUser) =>
				if err
					@main.log.error "Error creating BloggerUser", err, res
				else
					@createPost res, blogger, blog, blogInfo, post, bloggerUser, cb
		if !blog.lastGrab or publishDate > blog.lastGrab
			blog.lastGrab = publishDate
			blog.lastGrabRFC3339 = published
		blog
	createPost: (res, blogger, blog, blogInfo, post, bloggerUser, cb) ->
		{id, title, author, content, published, url} = post
		@main.database.Item.findOneAndUpdate
				source: "google-blogger"
				id: id
			,
				id: id
				title: title
				author: bloggerUser._id
				content: content
				publishDate: published
				link: url
				blogDetails:
					title: blogInfo.name
					url: blogInfo.url
			,
				upsert: true
			, (err, item) =>
				if err
					@main.log.error @, "Error inserting post", err, res
				else
					bloggerUser.items.push item
					bloggerUser.save (err) =>
						if err
							@main.log.error @, "Error pushing item onto bloggerUser",
								error, res,
									bloggerUser: bloggerUser
									item: item
					if cb
						cb
							blogger: blogger
							blogInfo: blogInfo
							blog: blog
							item: item
	anonAuth: (req, res, next) =>
		code = req.param "code"
		@main.google.anonClient().getToken code, (err, tokens) =>
			if err
				@main.log.error @,
					"Couldn't get OAuth2 tokens",
					err, res
			else
				@anonTokens = tokens
				res.redirect "/fetch/"
	anonBlogs: ->
		try 
			require "./blogs.json"
		catch e
			@main.log.warning @, "Couldn't load anon blog file", e
			blogs = []
	writeAnonBlogs: (blogs) ->
		@fs.writeFile "blogs.json", JSON.stringify blogs