class us.ryanleonard.CentralizedData.Config
	config:
		db:
			host: "localhost"
			table: "itemstore"
		web:
			port: 8080
		template:
			siteName: "GDRSD Posts"
			defaultPageName: "Overview"
			bloggerSubExplanation: "" + # "You can add additional blogs,"
				"such as a blog for a classroom project, a portfolio, or a blog as a teacher."
		overview:
			publishedCount: 25
			featuredCount: 15
		session:
			secret: "you might want to change this in production"
			checkExpirationsEvery: 120000
			cookie:
				path: "/"
				httpOnly: true
				maxAge: null
		supportEmail: "centralizeddata+bugreport@ryanleonard.us"
	constructor: (@main) ->
		@_ = require "underscore"
		@fs = require "fs"
		try
			@config = @_.extend @config, require "./config.json"
		catch e
			@main.log.warning @, "Couldn't load config file", e
	get: (section) ->
		@config[section]
	save: ->
		@fs.saveFile "config.json", JSON.stringify @config