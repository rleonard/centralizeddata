class us.ryanleonard.CentralizedData.Overview
	constructor: (@main) ->
		@main.router.get "/", @overviewPage
	overviewPage: (req, res, next) =>
		config = @main.config.get "overview"
		@main.database.Item.find({}).sort("-publishDate").limit(config.publishedCount).populate("author").exec (err, generalPosts) =>
			if err
				@main.log.error @, "Error fetching published items for the overview page", err, res
			else
				@main.database.Item.find({status: "featured"}).sort("-publishDate").limit(config.featuredCount).populate("author").exec (err, featuredPosts) =>
					if err
						@main.log.error @, "Error fetching featured items", err, res
					else
						res.render "overview",
							siteTemplate: @main.config.get "template"
							published: generalPosts
							featured: featuredPosts
							googleId: req.session.googleId
							displayName: req.session.displayName