class us.ryanleonard.CentralizedData.GoogleAPI
	constructor: (@main) ->
		@google = require "googleapis"
		@blogger = null
		@OAuth2 = @google.auth.OAuth2
	anonClient: ->
		config = @main.config.get "googleOAuth"
		new @OAuth2 config.id,
			config.secret,
			config.anonFetchRedirectURL
	userClient: ->
		config = @main.config.get "googleOAuth"
		new @OAuth2 config.id,
			config.secret,
			config.userLoginRedirectURL